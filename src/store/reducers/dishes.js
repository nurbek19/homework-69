import * as actionTypes from '../actions/actionTypes';

const initialState = {
    dishes: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.DISHES_SUCCESS:
            const dishes = [];
            for (let key in action.dishes) {
                dishes.unshift({...action.dishes[key], id: key});
            }
            return {...state, dishes: dishes};
        default:
            return state;
    }
};

export default reducer;