import * as actionTypes from '../actions/actionTypes';

const initialState = {
    foods: {},
    purchasing: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_FOOD:
            let foods = {...state.foods};

            if(!foods[action.food]) {
                foods[action.food] = {amount: 1, price: action.price};
            } else {
                foods[action.food].amount++;
            }
            return {
                ...state,
                foods
            };
        case actionTypes.REMOVE_FOOD:
            const orderedFoods = {...state.foods};
            delete orderedFoods[action.food];

            JSON.parse(JSON.stringify(orderedFoods));

            return {
                ...state,
                foods: orderedFoods
            };
        case actionTypes.SHOW_MODAL:
            return {
                ...state,
                purchasing: true
            };
        case actionTypes.CLOSE_MODAL:
            return {
                ...state,
                purchasing: false
            };
        case actionTypes.ORDER_SUCCESS:
            return {
                ...state,
                purchasing: false,
                foods: {}
            };
        default:
            return state;
    }
};

export default reducer;