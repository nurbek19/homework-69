import axios from '../../axios-dishes';
import * as actionTypes from './actionTypes';

export const addFood = (food, price) => {
  return {type: actionTypes.ADD_FOOD, food, price}
};

export const removeFood = food => {
  return {type: actionTypes.REMOVE_FOOD, food}
};

export const showModal = () => {
  return {type: actionTypes.SHOW_MODAL}
};

export const closeModal = () => {
  return {type: actionTypes.CLOSE_MODAL}
};

export const orderRequest = () => {
  return {type: actionTypes.ORDER_REQUEST}
};

export const orderSuccess = () => {
  return {type: actionTypes.ORDER_SUCCESS}
};

export const orderError = () => {
    return {type: actionTypes.ORDER_ERROR}
};

export const createOrder = order => {
  return dispatch => {
      dispatch(orderRequest());
      axios.post('/food-orders.json', order).then(() => {
          dispatch(orderSuccess());
      }, error => {
          dispatch(orderError());
      });
  }
};



