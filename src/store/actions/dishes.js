import axios from '../../axios-dishes';
import * as actionTypes from './actionTypes';

export const dishesRequest = () => {
  return {type: actionTypes.DISHES_REQUEST}
};

export const dishesSuccess = dishes => {
  return {type: actionTypes.DISHES_SUCCESS, dishes: dishes}
};

export const dishesError = () => {
    return {type: actionTypes.DISHES_ERROR}
};

export const getDishes = () => {
  return dispatch => {
      dispatch(dishesRequest());
      axios.get('/dishes.json').then(response => {
          dispatch(dishesSuccess(response.data));
      }, error => {
          dispatch(dishesError());
      });
  }
};