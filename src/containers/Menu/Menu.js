import React, {Component} from 'react';
import {connect} from 'react-redux';

import './Menu.css';
import {getDishes} from "../../store/actions/dishes";
import {addFood} from "../../store/actions/cart";

class Menu extends Component {
    componentDidMount() {
        this.props.getMenuItems();
    }

    render() {
        return (
            <div className="menu-items">
                {this.props.dishes.map(item => {
                    return (
                        <div className="menu-item" key={item.id}>
                            <div className="full-description">
                                <img src={item.img} alt="food"/>
                                <div className="item-description">
                                    <h4>{item.title}</h4>
                                    <p>{item.price}</p>
                                </div>
                            </div>
                            <button onClick={() => this.props.addToCart(item.title, item.price)}>Add to cart</button>
                        </div>
                    )
                })}
            </div>
        )
    }
}

const mapStateProps = state => {
    return {
        dishes: state.dishes.dishes
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getMenuItems: () => dispatch(getDishes()),
        addToCart: (food, price) => dispatch(addFood(food, price))
    }
};


export default connect(mapStateProps, mapDispatchToProps)(Menu);