import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import './Cart.css';
import {removeFood, createOrder, closeModal, showModal} from "../../store/actions/cart";
import Modal from '../../components/UI/Modal/Modal';
import OrderForm from '../../components/OrderForm/OrderForm';

class Cart extends Component {
    state = {
        address: '',
        name: '',
        phone: ''
    };

    valueChanged = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };


    purchaseHandler = () => {
        this.props.showModal();
    };

    purchaseCancelHandler = () => {
        this.props.closeModal();
    };


    render() {
        const foods = this.props.foods;
        let totalSum = 150;
        let dishes = [];

        for (let key in this.props.foods) {
            dishes.unshift({title: key, amount: foods[key].amount});
        }
        const order = {
            dishes: dishes,
            customer: {
                name: this.state.name,
                email: this.state.address,
                street: this.state.phone
            }
        };

        return (
            <Fragment>
                <Modal
                    show={this.props.purchasing}
                    closed={this.purchaseCancelHandler}
                >
                    <OrderForm
                        address={this.state.address}
                        name={this.state.name}
                        phone={this.state.phone}
                        changeValue={this.valueChanged}
                        submitOrder={() => this.props.createOrder(order)}
                    />
                </Modal>
                <div className="orders-container">
                    <div className="orders">
                        {Object.keys(foods).map(food => {
                            const foodAmount = foods[food].amount;
                            const foodPrice = foods[food].price;
                            totalSum += foodAmount * foodPrice;

                            return (
                                <div className="food" key={food}>
                                    <p onClick={() => this.props.removeFood(food)}>{food} &nbsp;x{foodAmount}</p>
                                    <p>{foodAmount * foodPrice} сом</p>
                                </div>
                            )
                        })}
                    </div>

                    <div className="total">
                        <p>
                            <span>Доставка</span>
                            <b>150</b>
                        </p>
                        <p>
                            <span>Итого</span>
                            <b>{totalSum}</b>
                        </p>
                    </div>

                    <button disabled={totalSum <= 150} onClick={this.purchaseHandler}>Place order</button>
                </div>
            </Fragment>
        )
    }
}

const mapStateProps = state => {
    return {
        foods: state.cart.foods,
        purchasing: state.cart.purchasing
    }
};

const mapDispatchToProps = dispatch => {
    return {
        removeFood: food => dispatch(removeFood(food)),
        createOrder: order => dispatch(createOrder(order)),
        closeModal: () => dispatch(closeModal()),
        showModal: () => dispatch(showModal()),
    }
};

export default connect(mapStateProps, mapDispatchToProps)(Cart);