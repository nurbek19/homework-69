import React from 'react';
import './OrderForm.css';

const OrderForm = props => {
  return(
      <div className="order-form">
          <input type="text" name="address" placeholder="Введите адрес" onChange={props.changeValue} value={props.address}/>
          <input type="text" name="name" placeholder="Введите имя" onChange={props.changeValue} value={props.name}/>
          <input type="text" name="phone" placeholder="Введите телефон" onChange={props.changeValue} value={props.phone}/>

          <button onClick={props.submitOrder}>Создать заказ</button>
      </div>
  )
};

export default OrderForm;